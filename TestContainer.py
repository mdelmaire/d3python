import unittest
from Container import *

class TestContainer(unittest.TestCase):
    def setUp(self):
        print "test"
 
    def tearDown(self):
        print "after"
    
    def test_getSizeX(self):
        container = Container(sizeX=200, sizeY=200)
        sizeX = container.getSizeX()
        self.assertEqual(sizeX, 200)
    
    if __name__=="__main__":
        unittest.main()
