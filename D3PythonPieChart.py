from D3PythonFigure import D3PythonFigure

class D3PythonPieChart(D3PythonFigure):
    
    def __init__(self, ax,  root):
        D3PythonFigure.__init__(self,ax,   root)
        self.xmlDecode(root)
       #explode = (0, 0.1, 0, 0)

        
    def __str__(self):
        return ""
        
    def xmlDecode(self,  pieChartRootNode):
        #Appel de la fonction dans la classe mere pour recuperer le titre
        D3PythonFigure.xmlDecode(self,  pieChartRootNode)
        seriesNode = pieChartRootNode.find("series")
        
        labels = []
        fracs=[]
        colors = []
        
        #We get all data
        for serie in seriesNode:
            fracs.append(serie.attrib["value"])
            labels.append(serie.attrib["label"])
            colors.append(serie.attrib["color"])
        self.ax.pie(fracs, labels=labels,  colors=colors, autopct='%1.1f%%', shadow=True, startangle=90)
        self.ax.axis('equal') #egalize ratio
        return self

    def draw(self,  plot):
        raise NotImplementedError
        return None
