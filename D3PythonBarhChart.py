from D3PythonFigure import D3PythonFigure
import xml.etree.ElementTree as ET
import numpy as np


class D3PythonBarhChart(D3PythonFigure):
    
    def __init__(self,  ax, root):       
        D3PythonFigure.__init__(self,ax,   root)
        self.xmlDecode(root)
        
    def xmlDecode(self, root):
        #Appel de la fonction dans la classe mere pour recuperer le titre
        D3PythonFigure.xmlDecode(self, root)
        
        x = root.find("x")
        y = root.find("y")
        
        xtitle = x.find("title").text
        ytitle = y.find("title").text
        
        ylabels = []
        for currentLabel in y.find("labels"):
            ylabels.append(currentLabel.text)
        
        values = []
        errors=[]
        for serie in root.find("series"):
            values.append(float(serie.text))
            errors.append(float(serie.attrib['error']))
        
        y_pos = np.arange(len(ylabels))
        
        self.ax.barh(y_pos,  values,  xerr=errors,  align='center',  alpha=0.4)
        self.ax.set_ylabel(ytitle)
        self.ax.set_xlabel(xtitle)
        self.ax.set_yticklabels(ylabels)
        return self        
        
            
    def draw(self,  plot):
        raise NotImplementedError
        return None
