class D3PythonFigure:
    
    def __init__(self, ax,  root):
        self.ax = ax
    
    #deparsage du xml
    def xmlDecode(self, root):
        self.title = root.find("title").text
        self.ax.set_title(self.title)
        return 0

    def draw(self,  plot):
        try:
            raise NotImplementedError
        except NotImplementedError as e:
            D3PythonLogger.getLogger().exception('wrong call of draw in D3PythonFigure')
            
        return None
