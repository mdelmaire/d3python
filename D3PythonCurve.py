from D3PythonFigure import D3PythonFigure
import xml.etree.ElementTree as ET
import numpy as np


class D3PythonCurve(D3PythonFigure):
    
    def __init__(self,  ax, root):
        D3PythonFigure.__init__(self,ax,   root)
        self.xmlDecode(root)
    
    def xmlDecode(self, root):
        #Appel de la fonction dans la classe mere pour recuperer le titre
        D3PythonFigure.xmlDecode(self, root)
        
        xValues = []
        yValues = []
        
        x = root.find("x")
        y = root.find("y")
        
        self.ax.set_ylabel(y.find("title").text)
        self.ax.set_xlabel(x.find("title").text)
        
        for serie in x.find("series"):
            xValues.append(float(serie.text))
        
        for serie in y.find("series"):
            yValues.append(float(serie.text))
            
        print xValues
        print yValues
        
        self.ax.plot(xValues,  yValues)
        return self
