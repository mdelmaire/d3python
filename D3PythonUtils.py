import xml.etree.ElementTree as ET
import os
import ftplib

class D3PythonUtils: #Classe regroupant quelques fonctions utilitaires au programme

    #deparsage du xml et renvoie les containers
    def decodeXML(self,  xmlFilePath):
        tree = ET.parse(xmlFilePath)
        root = tree.getroot()
        return self.getContainers(root)
        
    #Recupere les containers du fichier xml
    def getContainers(self,  root):
        currentDico = {}
        resultContainers = []
        containers = root.iter("container")
        for currentContainer in containers:
            figures = []
            container = {}
            container["attrs"] =  currentContainer.attrib
            currentFigures = currentContainer.iter("d3figure")
            for currentFigure in currentFigures:
                figures.append(currentFigure)
            container["figures"] = figures
            resultContainers.append(container)
        return resultContainers
    
    #Fonction permettant de se connecter en ftp
    def connectFtp(self,  adresseftp, nom='anonymous', mdpasse='anonymous@', passif=True):
        ftp = ftplib.FTP()
        ftp.connect(adresseftp)
        ftp.login(nom, mdpasse)
        ftp.set_pasv(passif)
        return ftp
    
    #Fonction permettant de fermer une connexion ftp
    def fermerftp(ftp):
        try:
            ftp.quit()
        except:
            ftp.close() 
        
    #Fonction permettant de telecharger un fichier via un acces ftp
    def downloadftp(self,  ftp, ficftp, repdsk='', ficdsk=None):
        if ficdsk==None:
            ficdsk=ficftp
        with open(os.path.join(repdsk, ficdsk), 'wb') as f:
            ftp.retrbinary('RETR ' + ficftp, f.write)
            

        
