from D3PythonFigure import D3PythonFigure

class D3PythonTable(D3PythonFigure):
    
    def __init__(self, title):
        D3PythonFigure.__init__(self,  title)
        
    def __str__(self):
        return ""
    
    def xmlDecode(self,  tableRootNode):
        return ""

    def draw(self,  plot):
        try:
            raise NotImplementedError
        except NotImplementedError as e:
            D3PythonLogger.getLogger().exception('wrong call of draw in D3PythonTable')
        
        return None
