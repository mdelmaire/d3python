from D3PythonException import D3PythonException

class D3PythonWrongGridSizeException(D3PythonException):
    def __init__(self, _message,  width,  height,  nbElements):
      D3PythonException.__init__(self, _message)
      self.width = width
      self.height = height
      self.nbElements = nbElements
