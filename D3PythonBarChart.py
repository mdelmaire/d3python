from D3PythonFigure import D3PythonFigure
import xml.etree.ElementTree as ET
import numpy as np

class D3PythonBarChart(D3PythonFigure):
    
    def __init__(self,  ax, root):
        D3PythonFigure.__init__(self,ax,   root)
        self.xmlDecode(root)
    
    def xmlDecode(self, root):
        #Appel de la fonction dans la classe mere pour recuperer le titre
        D3PythonFigure.xmlDecode(self,  root)
        x = root.find("x")
        y = root.find("y")
        seriesNode=  root.find("series")
        labels = x.find("labels")
        width =float( x.find("width").text)
        
        series=[]
        #add axes titles
        self.ax.set_ylabel(y.find("title").text)
        self.ax.set_xlabel(x.find("title").text)
        
        tmp = []
        for label in labels:
            tmp+=[label.text]
        self.ax.set_xticklabels(tuple(tmp))
        ind = np.arange(len(tmp))
       
        for serie in seriesNode:
            tmp = []
            tmp += [serie.find("title").text]
            values = []
            for value in serie.find("values"):
                values += [float(value.text)]
            tmp+=[tuple(values)]
            series+=[tmp]
            
        self.ax.set_xticks(ind+width)
        values = []
        titles = []
        for serie in series:
            values += serie[1]
            titles += serie[0]
            print width
            print serie[1]
            print ind+width
            self.ax.bar(ind+width, serie[1], width)
            
        #self.ax.legend( values,titles )
            
    def draw(self,  plot):
        raise NotImplementedError
        return None
