import logging

class D3PythonLogger:
    
    logger = None
    
    @staticmethod
    def getLogger():
        
        if D3PythonLogger.logger == None:
            D3PythonLogger.logger = logging.getLogger('D3Python')
            hdlr = logging.FileHandler('log/D3Python.log')
            formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
            hdlr.setFormatter(formatter)
            D3PythonLogger.logger.addHandler(hdlr) 
            D3PythonLogger.logger.setLevel(logging.INFO)
            
        return D3PythonLogger.logger 
