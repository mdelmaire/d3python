from D3PythonFigure import D3PythonFigure
import numpy as np

class D3PythonHistogram(D3PythonFigure):
    
    def __init__(self,  ax,  root):
        D3PythonFigure.__init__(self, ax,  root)
        self.xmlDecode(root)
        
    def xmlDecode(self,  root):
        #Appel de la fonction dans la classe mere pour recuperer le titre
        D3PythonFigure.xmlDecode(self, root)
        seriesNode = root.find("series")
        n_bins = int(root.find("bins").text)
        colors = []
        labels = []
        values = []
        for serie in seriesNode:
            cVal = []
            colors.append(serie.find("color").text)
            labels.append(serie.find("label").text)
            for value in serie.find("values"):
                cVal.append(float(value.text))
            values.append(cVal)
        self.ax.hist(values, n_bins, normed=1, histtype='bar', color=colors, label=labels)
        self.ax.legend(prop={'size': 10})
        return 0

    def draw(self,  plot):
        print "it works"
        return None
