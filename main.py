from D3PythonUtils import D3PythonUtils
from D3PythonLogger import D3PythonLogger
import matplotlib.pyplot as plt
import sys
import numpy as np
import xml.etree.ElementTree as ET
import pylab as P
import ftplib
import logging

from Container import Container
from D3PythonFigure import D3PythonFigure
from D3PythonHistogram import D3PythonHistogram
from D3PythonFTPException import D3PythonFTPException
        
XML_PATH = "xml.xml"
FTP_ATTR = "-f"
        
def main(argv=None):
    if argv is None :
        argv = sys.argv
        
    containers = []
    d3Utils = D3PythonUtils()
    ftp_mode = 0
    
    #On verifie si l utilisateur ne desire pas recuperer le fichier en FTP
    if argv[1] == FTP_ATTR:
        try:
            ftp = d3Utils.connectFtp(argv[2],  argv[3],  argv[4])
            d3Utils.downloadftp(ftp,  argv[5],  ficdsk=XML_PATH)
            tmpContainers =  d3Utils.decodeXML(XML_PATH)
            ftp_mode = 1
        except:
            raise D3PythonFTPException("error during ftp operations")
            
    else: #mode normal (sans ftp)
        tmpContainers =  d3Utils.decodeXML(argv[1])

    #On construit les containers ce qui entrainera la construction des figures
    for i in range(0, len(tmpContainers)):
        if ftp_mode == 1:
            currentContainer = Container(tmpContainers[i]["attrs"]["sizeX"],  tmpContainers[i]["attrs"]["sizeY"],  tmpContainers[i]["figures"],  argv[6]+"/" +tmpContainers[i]["attrs"]["name"]+".jpg")
        else:
            currentContainer = Container(tmpContainers[i]["attrs"]["sizeX"],  tmpContainers[i]["attrs"]["sizeY"],  tmpContainers[i]["figures"],  argv[2]+"/" +tmpContainers[i]["attrs"]["name"]+".jpg")
        containers.append(currentContainer)
        if currentContainer.getNbFigure() != 0:
            #On sauvegarde les figures en image
            currentContainer.save()
            #On affiche les figures
            currentContainer.show()
            
    return 0 
    
if __name__ == "__main__":
    D3PythonLogger.getLogger().info("exectute the program")
    main()
    D3PythonLogger.getLogger().info("the program ended correctly")

