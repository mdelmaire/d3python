import numpy as np
import unittest
import matplotlib.pyplot as plt
from D3PythonLogger import D3PythonLogger
from D3PythonHistogram import D3PythonHistogram
from D3PythonBarChart import D3PythonBarChart
from D3PythonPieChart import D3PythonPieChart
from D3PythonBarhChart import D3PythonBarhChart
from D3PythonCurve import D3PythonCurve

import sys
sys.path.append("exception/")
from D3PythonWrongGridSizeException import *

import math

class Container : 
    
    #Constructor
    def __init__(self,  sizeX=500,  sizeY=500,  xmlFigures=None,  fileName=None):
        self._sizeX = sizeX
        self._sizeY = sizeY
        self._fileName = fileName
        self._xmlFigures = xmlFigures
        if xmlFigures != None:
            self._dimensions = self.getGridSize(len(xmlFigures))
            self._figures = self.initFigures()
        
    #Getters
    def _getXmlFigures(self):
        return self._xmlFigures
        
    def _getSizeX(self):
        return self._sizeX

    def _getSizeY(self):
        return self._sizeY
        
    def _getFileName(self):
        return self._fileName
        
    def getNbFigure(self):
        return len(self._figures)

    def _getFigures(self):
        return self._figures
        
    def _getAxes(self):
        return self._figures
    
    def _getDimension(self):
        return self._dimensions
        
    #Setters
    def _setFileName(self,  fileName):
        self._fileName = fileName
        return self
    
    def _setXmlFigures(self,  figures):
        self._xmlFigures = figures
        return self
    
    def _setSizeX(self,  sizeX):
        self._sizeX = sizeX
        return self
        
    def _setSizeY(self,  sizeY):
        self._sizeY = sizeY
        return self
    
    def _setFigures(self,  figures):
        self._figures = figures
        self._dimensions = getGridSize(len(figures))
        return self

    def _setAxes(self,  axes):
        self._figures = axes
        return self

    def _setDimension(self,  width, height):
        self._dimensions = width,  height
        return self
        
    
    #Utils
    def xmlDecode(self,  xml):
        return 0
    
    #Like toString() in Java
    def __str__(self):
        return "Container {0}".format(self.title)
    
    
    #renvoie le nombre de lignes et le nombre de colonnes que l on doit definir dans notre plot
    def getGridSize(self,  nbElements):
        width  =math.ceil(math.sqrt(nbElements))
        height = math.ceil(nbElements/width)
        D3PythonLogger.getLogger().info('creating a '+str(width)+' by '+str(height)+' grid')
        if width * height < nbElements:
            raise D3PythonWrongGridSizeException("wrong grid size with "+str(width)+" and "+str(height),  width,  height,  nbElements)
        return int(width),  int(height)
    
    #Call right constructor
    def initFigures(self):
        #read parse and fill _figures with abstract figures
        figures = []
        
        axes = []
        width,  height = self.getGridSize(len(self._xmlFigures))
        print width
        print height
        for i in range(width):
            for j in range(height):
                axes.append(plt.subplot2grid((width, height),  (i, j),  colspan=1 ))
        
        index = 0
        for currentFigure in self._xmlFigures:
            currentType = currentFigure.attrib["type"]
            currentAxe = axes[index]
            #On appelle le bon constructeur en fonction du type de la figure
            D3PythonLogger.getLogger().info('creating a '+ currentType)
            if currentType == "histogram":
                figures.append(D3PythonHistogram(currentAxe,  currentFigure))
            elif currentType == "barChart":
                figures.append(D3PythonBarChart(currentAxe,  currentFigure))
            elif currentType == "pieChart":
                figures.append(D3PythonPieChart(currentAxe,  currentFigure))
            elif currentType == "barhChart":
                figures.append(D3PythonBarhChart(currentAxe,  currentFigure))
            elif currentType == "curve":
                figures.append(D3PythonCurve(currentAxe,  currentFigure))
            index = index +1
        return figures
    
    
    def show(self): 
        plt.show()
        return self
    
    def save(self):
        #On sauvegarde l image cree dans le fileName
        plt.savefig(self.fileName)
        return self
    
    #Properties
    fileName = property(_getFileName,  _setFileName)
    axes = property(_getAxes,  _setAxes)
    figures = property(_getFigures,  _setFigures)
    xmlFigures = property(_getXmlFigures,  _setXmlFigures)
    sizeX = property(_getSizeX,  _setSizeX)
    sizeY = property(_getSizeY,  _setSizeY)
    
    
#Tests de la classe Container
class TestContainer(unittest.TestCase):
    def setUp(self):
        self.container = Container(sizeX=200, sizeY=200,  fileName="Test")
 
    def tearDown(self):
        return 0
    
    def test_getSizeX(self):
        self.assertEqual(self.container.sizeX, 200)
    
    def test_getSizeY(self):
        self.assertEqual(self.container.sizeY,  200)
    
    def test_getFileName(self):
        self.assertEqual(self.container.fileName,  "Test")
    
    def test_setSizeX(self):
        self.container.sizeX = 300
        self.assertEqual(self.container.sizeX,  300)
    
    def test_setSizeY(self):
        self.container.sizeY = 300
        self.assertEqual(self.container.sizeY, 300)
    
    def test_setFileName(self):
        self.container.fileName = "test2"
        self.assertEqual(self.container.fileName, "test2")        

if __name__=="__main__":
    unittest.main()
    
